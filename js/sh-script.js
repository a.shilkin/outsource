let seeMoreOpen = document.querySelector('.sh-see-more');
let seeMoreClose = document.querySelector('.sh-see-more_down');

let shDesktopElementClass = document.querySelectorAll('.sh-desctop-element');

let thirdBlockItems = document.querySelector('.third-block-items');

let fourBlock = document.querySelector('.four-block');
let fifthBlock = document.querySelector('.fifth-block');
let fifthZeroBlock = document.querySelector('.fifth-zero-block');
let sixthBlock = document.querySelector('.sixth-block');
let seventhBlock = document.querySelector('.seventh-block');
let twelveBlock = document.querySelector('.twelve-block');
let thirteenthBlockIcons = document.querySelector('.thirteenth-block-icons');
let footerBlock = document.querySelector('.footer-block');


seeMoreOpen.addEventListener('click', () => {
    shDesktopElementClass.forEach(el => el.style.setProperty("display", "flex", "important"));
    thirdBlockItems.style.height = '5661' + 'px';
    seeMoreOpen.style.display = 'none';
    seeMoreClose.style.display = 'flex';

    fourBlock.style.top = '7010' + 'px';
    fifthZeroBlock.style.top = '7064' + 'px';
    fifthBlock.style.top = '7122' + 'px';
    sixthBlock.style.top = '7273' + 'px';
    seventhBlock.style.top = '7274' + 'px';
    twelveBlock.style.top = '7297' + 'px';
    thirteenthBlockIcons.style.top = '7330' + 'px';
    footerBlock.style.top = '7330' + 'px';
});

seeMoreClose.addEventListener('click', () => {
    shDesktopElementClass.forEach(el => el.style.setProperty("display", "none", "important"));
    thirdBlockItems.style.height = '1754' + 'px';
    seeMoreOpen.style.display = 'flex';
    seeMoreOpen.style.top = '190' + 'px';
    seeMoreClose.style.display = 'none';

    fourBlock.style.top = '3061' + 'px';
    fifthZeroBlock.style.top = '3100' + 'px';
    fifthBlock.style.top = '3154' + 'px';
    sixthBlock.style.top = '3300' + 'px';
    seventhBlock.style.top = '3250' + 'px';
    twelveBlock.style.top = '3270' + 'px';
    thirteenthBlockIcons.style.top = '3303' + 'px';
    footerBlock.style.top = '3303' + 'px';
});


// ! skill bars script

(function () {
    var skillbar = document.querySelector('.sh-html');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('progress-line');
            }
        });
    });

    observer.observe(skillbar);
})();

// !

(function () {
    var skillbar2 = document.querySelector('.sh-html-two');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('progress-line');
            }
        });
    });

    observer.observe(skillbar2);
})();

